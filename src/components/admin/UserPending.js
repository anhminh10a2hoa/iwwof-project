import React, { useEffect, useState } from 'react'
import MenuAppBar from '../UI/MenuAppBar'
import { useSelector } from 'react-redux'
import Loading from '../UI/Loading'
import axios from 'axios'
import PersonAddIcon from '@material-ui/icons/PersonAdd'
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled'
import DeleteIcon from '@material-ui/icons/Delete'
import { BASE_URL } from '../../constance/url'

function UserPending() {
	const userInfo = useSelector((state) => state.users.userInfo)
	const [listUser, setListUser] = useState([])
	const [loading, setLoading] = useState(true)

	useEffect(() => {
		axios
			.get(BASE_URL + 'admin/get/all/user', {
				headers: {
					Authorization: 'Bearer ' + userInfo.accessToken,
				},
			})
			.then((res) => {
				res.data.sort(function (x, y) {
					return x.allowByAdmin === y.allowByAdmin ? 0 : x ? -1 : 1
				})
				setListUser(res.data)
				setLoading(false)
			})
	}, [])

	const acceptUser = (id) => {
		axios
			.post(
				BASE_URL + 'admin/approve/user',
				JSON.stringify({
					id: id,
				}),
				{
					headers: {
						Authorization: 'Bearer ' + userInfo.accessToken,
						'Content-Type': 'application/json;charset=UTF-8',
					},
				}
			)
			.then((res) => {
				if (res.status === 200) {
					new Promise(() => setTimeout(alert('Approve successfully!'), 2000))
					window.location.reload()
				} else {
					alert('Approve failed, try again!')
				}
			})
			.catch(() => {
				alert('Approve failed, try again!')
			})
	}

	const deleteUser = (id) => {
		console.log(id)
		axios
			.delete(BASE_URL + 'admin/delete/user', {
				headers: {
					Authorization: 'Bearer ' + userInfo.accessToken,
					'Content-Type': 'application/json;charset=UTF-8',
				},
				data: id,
			})
			.then((res) => {
				if (res.status === 200) {
					new Promise(() => setTimeout(alert('Delete successfully!'), 2000))
					window.location.reload()
				} else {
					alert('Delete failed, try again!')
				}
			})
			.catch(() => {
				alert('Delete failed, try again!')
			})
	}

	return loading ? (
		<div>
			<MenuAppBar title='admin' />
			<Loading />
		</div>
	) : (
		<div className='user-pending-list'>
			<MenuAppBar title='admin' />
			<h1 className='title title-pending-list'>
				{listUser.length + ' '} users both active users and inactive users
			</h1>
			<table>
				<thead>
					<tr>
						<th>Username</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{listUser.map((user) => (
						<tr key={user.id}>
							<td>{user.username}</td>
							<td>{user.email}</td>
							<td>
								{!user.allowByAdmin ? (
									<React.Fragment>
										<PersonAddIcon
											className='accept_button'
											onClick={() => acceptUser(user.id)}
										>
											Accept
										</PersonAddIcon>
										<PersonAddDisabledIcon
											className='delete_button'
											onClick={() => deleteUser(user.id)}
										>
											Delete
										</PersonAddDisabledIcon>
									</React.Fragment>
								) : (
									<DeleteIcon
										className='delete_button'
										onClick={() => deleteUser(user.id)}
									>
										Delete
									</DeleteIcon>
								)}
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	)
}

export default UserPending
