import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import HomeIcon from '@material-ui/icons/Home'
import DescriptionIcon from '@material-ui/icons/Description'
import PersonIcon from '@material-ui/icons/Person'
import LanguageIcon from '@material-ui/icons/Language'
import DoneIcon from '@material-ui/icons/Done'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import EmailIcon from '@material-ui/icons/Email'
import { useSelector } from 'react-redux'
import { BASE_URL } from '../../constance/url'
import axios from 'axios'

function upperCaseFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1)
}

export default function AlertDialog({ open, businessInfo, handleClose }) {
	const roles = useSelector((state) => state.users.roles)
	const userInfo = useSelector((state) => state.users.userInfo)

	const deleteBusinessHandle = () => {
		axios
			.delete(BASE_URL + 'business/delete', {
				headers: {
					Authorization: 'Bearer ' + userInfo.accessToken,
					'Content-Type': 'application/json;charset=UTF-8',
				},
				data: businessInfo.id,
			})
			.then((res) => {
				if (res.status === 200) {
					new Promise(() => setTimeout(alert('Delete successfully!'), 2000))
					window.location.reload()
				} else {
					alert('Delete failed, try again!')
				}
			})
			.catch(() => {
				alert('Delete failed, try again!')
			})
	}

	return (
		<div>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby='alert-dialog-title'
				aria-describedby='alert-dialog-description'
				className='dialog-container'
			>
				<DialogTitle id='alert-dialog-title'>
					<HomeIcon className='icon' /> {businessInfo.brandName}
				</DialogTitle>
				<DialogContent>
					<DialogContentText id='alert-dialog-description'>
						<DescriptionIcon className='icon' />{' '}
						{upperCaseFirstLetter(businessInfo.description)}
					</DialogContentText>
				</DialogContent>
				<DialogContent>
					<DialogContentText id='alert-dialog-description'>
						<DoneIcon className='icon' />{' '}
						{upperCaseFirstLetter(businessInfo.additionalOffer)}
					</DialogContentText>
				</DialogContent>
				<DialogContent>
					<LanguageIcon className='icon' />{' '}
					<a href={businessInfo.websites} target='_blank' rel='noreferrer'>
						{businessInfo.websites}
					</a>
				</DialogContent>
				<DialogContent>
					<PersonIcon className='icon' /> {businessInfo.user.username}{' '}
					<EmailIcon className='icon' />{' '}
					<a href={'mailto:' + businessInfo.user.email}>
						{businessInfo.user.email}
					</a>
				</DialogContent>
				<DialogContent>
					<LocationOnIcon className='icon' /> {businessInfo.location}
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color='primary'>
						Close
					</Button>
					{(roles[0] === 'ROLE_ADMIN' ||
						userInfo.id === businessInfo.user.id) && (
						<Button onClick={deleteBusinessHandle} color='primary'>
							Delete
						</Button>
					)}
				</DialogActions>
			</Dialog>
		</div>
	)
}
