import React, { useEffect } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import PersonIcon from '@material-ui/icons/Person'
import LanguageIcon from '@material-ui/icons/Language'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import EmailIcon from '@material-ui/icons/Email'
import PhoneIcon from '@material-ui/icons/Phone'

export default function UserDialog({ open, userInfo, handleClose }) {
	return (
		<div>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby='alert-dialog-title'
				aria-describedby='alert-dialog-description'
				className='dialog-container'
			>
				<DialogTitle id='alert-dialog-title'>
					<PersonIcon className='icon' /> {userInfo.username}
				</DialogTitle>
				<DialogContent>
					<LanguageIcon className='icon' />{' '}
					<a href={userInfo.website} target='_blank' rel='noreferrer'>
						{userInfo.website ? userInfo.website : 'Unknown'}
					</a>
				</DialogContent>
				<DialogContent>
					<EmailIcon className='icon' />{' '}
					<a href={'mailto:' + userInfo.email}>{userInfo.email}</a>
				</DialogContent>
				<DialogContent>
					<LocationOnIcon className='icon' />{' '}
					{userInfo.location ? userInfo.location : 'Unknown'}
				</DialogContent>
				<DialogContent>
					<PhoneIcon className='icon' />{' '}
					{userInfo.phoneNumber ? userInfo.phoneNumber : 'Unknown'}
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color='primary'>
						Close
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}
