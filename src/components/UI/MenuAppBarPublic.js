import React from 'react'
import { fade, makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import { useHistory } from 'react-router-dom'
import GroupIcon from '@material-ui/icons/Group'
import BusinessIcon from '@material-ui/icons/Business'
import Tooltip from '@material-ui/core/Tooltip'
import HomeIcon from '@material-ui/icons/Home'
import Logo from '../../assets/logo.png'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	appBar: {
		backgroundColor: '#DA1984',
	},
	logo: {
		height: '50px',
		position: 'absolute',
		width: '50px',
		backgroundColor: '#fff',
		borderRadius: '25px',
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
		marginLeft: theme.spacing(8),
	},
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing(2),
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(3),
			width: 'auto',
		},
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	inputRoot: {
		color: 'inherit',
	},
	inputInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('md')]: {
			width: '20ch',
		},
	},
}))

export default function MenuAppBarPublic() {
	return <MenuAppBarTemplate />
}

const MenuAppBarTemplate = () => {
	const classes = useStyles()
	let history = useHistory()

	const handleBusiness = () => {
		history.push('/public/businesses')
	}

	const handleUser = () => {
		history.push('/public/users')
	}

	return (
		<AppBar position='sticky' top='0' className={classes.appBar}>
			<Toolbar>
				<Typography variant='h6' className={classes.title}>
					Public
				</Typography>
				<img src={Logo} alt='' className={classes.logo} />
				<Tooltip title='Login' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={() => {
							history.push('/login')
						}}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<HomeIcon />
					</IconButton>
				</Tooltip>
				<Tooltip title='Businesses page' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleBusiness}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<BusinessIcon />
					</IconButton>
				</Tooltip>
				<Tooltip title='Users page' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleUser}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<GroupIcon />
					</IconButton>
				</Tooltip>
			</Toolbar>
		</AppBar>
	)
}
