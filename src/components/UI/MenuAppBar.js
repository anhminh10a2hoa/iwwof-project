import React from 'react'
import { fade, makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import AccountCircle from '@material-ui/icons/AccountCircle'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import { setRoles, setUser, setSearchValue } from '../../actions/userAction'
import { useHistory, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import ListIcon from '@material-ui/icons/List'
import GroupIcon from '@material-ui/icons/Group'
import BusinessIcon from '@material-ui/icons/Business'
import Tooltip from '@material-ui/core/Tooltip'
import AddIcon from '@material-ui/icons/Add'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import HomeIcon from '@material-ui/icons/Home'
import Logo from '../../assets/logo.png'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	appBar: {
		backgroundColor: '#DA1984',
	},
	logo: {
		height: '50px',
		position: 'absolute',
		width: '50px',
		backgroundColor: '#fff',
		borderRadius: '25px',
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
		marginLeft: theme.spacing(8),
	},
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing(2),
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(3),
			width: 'auto',
		},
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	inputRoot: {
		color: 'inherit',
	},
	inputInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('md')]: {
			width: '20ch',
		},
	},
}))

export default function MenuAppBar(props) {
	const userRole = useSelector((state) => state.users.roles)

	return userRole.includes('ROLE_ADMIN') ? (
		<MenuAppBarTemplate title={props.title} isAdmin={true} />
	) : userRole.includes('ROLE_USER') ? (
		<MenuAppBarTemplate title={props.title} isAdmin={false} />
	) : (
		<Redirect to='/login' />
	)
}

const MenuAppBarTemplate = ({ title, isAdmin }) => {
	const classes = useStyles()
	let history = useHistory()
	const location = useLocation()
	const dispatch = useDispatch()
	const searchingValueHandler = (event) => {
		const { value } = event.target
		dispatch(setSearchValue(value))
	}
	const handleMenu = () => {
		history.push('/me')
	}

	const handleLogOut = () => {
		localStorage.removeItem('user')
		dispatch(setRoles([]))
		dispatch(setUser({}))
		history.push('/login')
	}

	const handleBusiness = () => {
		history.push('/business')
	}

	const handleUser = () => {
		history.push('/users')
	}

	const handleList = () => {
		history.push('/admin')
	}

	const handleAddBusiness = () => {
		history.push('/add-business')
	}

	return (
		<AppBar position='sticky' top='0' className={classes.appBar}>
			<Toolbar>
				<Typography variant='h6' className={classes.title}>
					{title.toUpperCase()}
				</Typography>
				<img src={Logo} alt='' className={classes.logo} />
				<Tooltip title='Main Page' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={() => {
							history.push('/')
						}}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<HomeIcon />
					</IconButton>
				</Tooltip>
				<Tooltip title='Add your business' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleAddBusiness}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<AddIcon />
					</IconButton>
				</Tooltip>
				<Tooltip title='Businesses page' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleBusiness}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<BusinessIcon />
					</IconButton>
				</Tooltip>
				{location.pathname.includes('/business') && (
					<div className={classes.search}>
						<div className={classes.searchIcon}>
							<SearchIcon />
						</div>
						<InputBase
							placeholder='Search…'
							classes={{
								root: classes.inputRoot,
								input: classes.inputInput,
							}}
							inputProps={{ 'aria-label': 'search' }}
							onChange={searchingValueHandler}
						/>
					</div>
				)}
				<Tooltip title='Users page' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleUser}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<GroupIcon />
					</IconButton>
				</Tooltip>
				{isAdmin && (
					<Tooltip title='All users list' arrow>
						<IconButton
							aria-label='account of current user'
							aria-controls='menu-appbar'
							aria-haspopup='true'
							onClick={handleList}
							color='inherit'
							className={classes.accountCircleIcon}
						>
							<ListIcon />
						</IconButton>
					</Tooltip>
				)}
				<Tooltip title='Your account' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleMenu}
						color='inherit'
						className={classes.accountCircleIcon}
					>
						<AccountCircle />
					</IconButton>
				</Tooltip>
				<Tooltip title='Log out' arrow>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						onClick={handleLogOut}
						color='inherit'
					>
						<ExitToAppIcon />
					</IconButton>
				</Tooltip>
			</Toolbar>
		</AppBar>
	)
}
