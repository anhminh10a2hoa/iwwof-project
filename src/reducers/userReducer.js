import { SET_ROLE, SET_USER_INFO, SEARCHING_VALUE } from '../actions/types'
import jwt_decode from 'jwt-decode'

let checkUser = false
let user = JSON.parse(localStorage.getItem('user'))
if (user) {
	const token = jwt_decode(user.accessToken, { complete: true })
	if (token.exp * 1000 < new Date().getTime()) {
		localStorage.removeItem('user')
		checkUser = false
	} else {
		checkUser = true
	}
}

const initialState = {
	roles: checkUser ? user.roles : [],
	userInfo: checkUser ? user : {},
	searchValue: '',
}

export default function (state = initialState, action) {
	switch (action.type) {
		case SET_ROLE:
			return {
				...state,
				roles: action.payload,
			}
		case SET_USER_INFO:
			return {
				...state,
				userInfo: action.payload,
			}
		case SEARCHING_VALUE:
			return {
				...state,
				searchValue: action.payload,
			}
		default:
			return state
	}
}
