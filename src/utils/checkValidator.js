export function checkSignUp(userSignUp) {
	if (userSignUp.username.length < 8 && userSignUp.password.length < 8) {
		alert(
			'Your password and your user name must be at least 8 characters, try again.'
		)
		return
	}
	if (userSignUp.password !== userSignUp.confirmPassword) {
		alert('Your password does not match!, try again.')
		return
	}
}
