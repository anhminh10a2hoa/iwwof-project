import { SET_ROLE, SET_USER_INFO, SEARCHING_VALUE } from '../actions/types'
import axios from 'axios'
import { BASE_URL } from '../constance/url'

export const setAuth = (username, password) => (dispatch) => {
	if (username.length === 0 && password.length === 0) {
		alert('Please enter the Username and Password!')
	} else {
		axios({
			method: 'POST',
			url: BASE_URL + 'auth/signin',
			data: JSON.stringify({
				username,
				password,
			}),
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
		})
			.then((res) => {
				if (res.status === 200) {
					dispatch({
						type: SET_ROLE,
						payload: res.data.roles,
					})
					dispatch({
						type: SET_USER_INFO,
						payload: res.data,
					})
					localStorage.setItem('user', JSON.stringify(res.data))
				} else {
					alert('Sign in failed, try again!')
				}
			})
			.catch(() => {
				alert('Sign in failed, try again!')
			})
	}
}

export const setRoles = (roles) => (dispatch) => {
	dispatch({
		type: SET_ROLE,
		payload: roles,
	})
}

export const setUser = (user) => (dispatch) => {
	dispatch({
		type: SET_USER_INFO,
		payload: user,
	})
}

export const setSearchValue = (string) => (dispatch) => {
	dispatch({
		type: SEARCHING_VALUE,
		payload: string,
	})
}
