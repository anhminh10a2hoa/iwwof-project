import React, { useEffect } from 'react'
import './App.scss'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import ComeBack from './pages/ComeBack'
import NotFound from './pages/NotFound'
import Business from './pages/Business'
import AddBusiness from './pages/AddBusiness'
import ForgetPassword from './pages/ForgetPassword'
import UpdatePassword from './pages/UpdatePassword'
import PublicPageUsers from './pages/PublicPageUsers'
import PublicPageBusinesses from './pages/PublicPageBusinesses'
import MainPage from './pages/MainPage'
import Users from './pages/Users'
import Me from './pages/Me'
import UserPending from './components/admin/UserPending'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
import { useDispatch } from 'react-redux'
import { setRoles, setUser } from './actions/userAction'

function App() {
	const dispatch = useDispatch()
	useEffect(() => {
		let user = JSON.parse(localStorage.getItem('user'))
		if (user) {
			const token = jwt_decode(user.accessToken, { complete: true })
			if (token.exp * 1000 < new Date().getTime()) {
				localStorage.removeItem('user')
				dispatch(setRoles([]))
				dispatch(setUser({}))
			} else {
				dispatch(setRoles(user.roles))
				dispatch(setUser(user))
			}
		}
	}, [])

	return (
		<div className='App'>
			<BrowserRouter>
				<Switch>
					<Route path='/login' exact component={SignIn} />
					<Route path='/signup' exact component={SignUp} />
					<Route path='/admin' component={UserPending} />
					<Route path='/business' component={Business} />
					<Route path='/add-business' component={AddBusiness} />
					<Route path='/users' exact component={Users} />
					<Route path='/' exact component={MainPage} />
					<Route path='/forgot-password' exact component={ForgetPassword} />
					<Route path='/update-password' exact component={UpdatePassword} />
					<Route path='/public/users' exact component={PublicPageUsers} />
					<Route
						path='/public/businesses'
						exact
						component={PublicPageBusinesses}
					/>
					<Route path='/me' exact component={Me} />
					<Route path='/401' exact component={ComeBack} />
					<Route path='/404' component={NotFound} />
					<Redirect to='/404' />
				</Switch>
			</BrowserRouter>
		</div>
	)
}

export default App
