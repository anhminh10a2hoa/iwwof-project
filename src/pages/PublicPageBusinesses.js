import React, { useEffect, useState } from 'react'
import MenuAppBarPublic from '../components/UI/MenuAppBarPublic'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import AlertDialog from '../components/UI/AlertDialog'
import { BASE_URL } from '../constance/url'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: '#6886c5',
		'&:hover': {
			backgroundColor: '#f9f9f9',
		},
	},
}))

function Business() {
	const classes = useStyles()
	const [listBusiness, setListBusiness] = useState([])
	const [cate, setCate] = useState([])
	const [listSearchBusiness, setListSearchBusiness] = useState([])
	const [loading, setLoading] = useState(true)
	const [isOpenDialog, setIsOpenDialog] = useState(false)
	const [businessInfo, setBusinessInfo] = useState(1)
	const [category, setCategory] = useState('Choose category')

	useEffect(() => {
		axios
			.get(BASE_URL + 'public/business/all')
			.then((res) => {
				setListBusiness(res.data)
				let arrayCat = []
				res.data.map((c) => {
					arrayCat.push(c.category)
				})
				setCate(arrayCat)
			})
			.then(() => {
				setLoading(false)
			})
	}, [])

	useEffect(() => {
		if (category !== 'Choose category') {
			let tempArray = listBusiness.filter((b) => b.category === category)
			setListSearchBusiness(tempArray)
		}
	}, [category])

	const handleClose = () => {
		setIsOpenDialog(false)
	}

	const moreDetailsHandlers = (id) => {
		const business = listBusiness.find((b) => b.id == id)
		setBusinessInfo(business)
		setIsOpenDialog(true)
	}

	const onChangeCategory = (event) => {
		setCategory(event.target.value)
	}

	return (
		<div>
			<MenuAppBarPublic />
			<h1 className='title'>
				{listBusiness.length + ' '} businesses are available now
			</h1>
			<div className='select-container'>
				<select
					name='category'
					id='category'
					onChange={onChangeCategory}
					value={category}
				>
					<option>Choose category</option>
					{cate.map((c) => (
						<option value={c}>{c}</option>
					))}
				</select>
			</div>
			<div className='business-container'>
				<Grid container spacing={1}>
					<Grid container item lg={12} md={12} sm={12} spacing={6}>
						{category === 'Choose category'
							? listBusiness.map((business) => (
									<Grid
										item
										lg={3}
										md={4}
										sm={12}
										className='business-item'
										key={business.id}
									>
										<Paper className={classes.paper}>
											<img
												className='business-image'
												src='https://cdn.pixabay.com/photo/2016/02/19/11/19/office-1209640_960_720.jpg'
												alt='office'
											/>
											<h2>{business.brandName}</h2>
											<p className='text'>Location: {business.location}</p>
											<Button
												variant='contained'
												color='secondary'
												onClick={() => moreDetailsHandlers(business.id)}
											>
												More details
											</Button>
										</Paper>
									</Grid>
							  ))
							: listSearchBusiness.map((business) => (
									<Grid
										item
										lg={3}
										md={4}
										sm={12}
										className='business-item'
										key={business.id}
									>
										<Paper className={classes.paper}>
											<img
												className='business-image'
												src='https://cdn.pixabay.com/photo/2016/02/19/11/19/office-1209640_960_720.jpg'
												alt='office'
											/>
											<h2>{business.brandName}</h2>
											<p className='text'>Location: {business.location}</p>
											<Button
												variant='contained'
												color='secondary'
												onClick={() => moreDetailsHandlers(business.id)}
											>
												More details
											</Button>
										</Paper>
									</Grid>
							  ))}
					</Grid>
				</Grid>
				{isOpenDialog && (
					<AlertDialog
						open={isOpenDialog}
						businessInfo={businessInfo}
						handleClose={handleClose}
					/>
				)}
			</div>
		</div>
	)
}

export default Business
