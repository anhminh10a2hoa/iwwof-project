import React, { useEffect, useState } from 'react'
import MenuAppBar from '../components/UI/MenuAppBar'
import { useSelector } from 'react-redux'
import Loading from '../components/UI/Loading'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import AlertDialog from '../components/UI/AlertDialog'
import { BASE_URL } from '../constance/url'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: '#6886c5',
		'&:hover': {
			backgroundColor: '#f9f9f9',
		},
	},
}))

function Business() {
	const classes = useStyles()
	const userInfo = useSelector((state) => state.users.userInfo)
	const [listBusiness, setListBusiness] = useState([])
	const [listSearchBusiness, setListSearchBusiness] = useState([])
	const [loading, setLoading] = useState(true)
	const [isOpenDialog, setIsOpenDialog] = useState(false)
	const [businessInfo, setBusinessInfo] = useState(1)
	const searchValue = useSelector((state) => state.users.searchValue)
	const [category, setCategory] = useState('Choose category')
	const [cate, setCate] = useState([])

	useEffect(() => {
		axios
			.get(BASE_URL + 'business/get/all', {
				headers: {
					Authorization: 'Bearer ' + userInfo.accessToken,
				},
			})
			.then((res) => {
				setListBusiness(res.data)
				let arrayCat = []
				res.data.map((c) => {
					arrayCat.push(c.category)
				})
				setCate(arrayCat)
			})
			.then(() => {
				setLoading(false)
			})
	}, [])

	useEffect(() => {
		if (searchValue.length > 0) {
			let tempArray = listBusiness.filter(
				(b) =>
					b.location.includes(searchValue) || b.category.includes(searchValue)
			)
			setListSearchBusiness(tempArray)
		}
	}, [searchValue])

	useEffect(() => {
		if (category !== 'Choose category') {
			let tempArray = listBusiness.filter((b) => b.category === category)
			setListSearchBusiness(tempArray)
		}
	}, [category])

	const handleClose = () => {
		setIsOpenDialog(false)
	}

	const moreDetailsHandlers = (id) => {
		const business = listBusiness.find((b) => b.id == id)
		setBusinessInfo(business)
		setIsOpenDialog(true)
	}

	const onChangeCategory = (event) => {
		setCategory(event.target.value)
	}

	return loading ? (
		<div>
			<MenuAppBar title='admin' />
			<Loading />
		</div>
	) : (
		<div>
			<MenuAppBar title='Business' />
			<h1 className='title'>Your Businesses</h1>
			<div className='select-container'>
				<select
					name='category'
					id='category'
					onChange={onChangeCategory}
					value={category}
				>
					<option>Choose category</option>
					{cate.map((c) => (
						<option value={c}>{c}</option>
					))}
				</select>
			</div>
			<div className='business-container'>
				<Grid container spacing={1}>
					<Grid container item lg={12} md={12} sm={12} spacing={6}>
						{searchValue.length === 0 && category === 'Choose category'
							? listBusiness.map((business) => (
									<Grid
										item
										lg={3}
										md={4}
										sm={12}
										className='business-item'
										key={business.id}
									>
										<Paper className={classes.paper}>
											<img
												className='business-image'
												src='https://cdn.pixabay.com/photo/2016/02/19/11/19/office-1209640_960_720.jpg'
												alt='office'
											/>
											<h2>{business.brandName}</h2>
											<p className='text'>Location: {business.location}</p>
											<Button
												variant='contained'
												color='secondary'
												onClick={() => moreDetailsHandlers(business.id)}
											>
												More details
											</Button>
										</Paper>
									</Grid>
							  ))
							: listSearchBusiness.map((business) => (
									<Grid
										item
										lg={3}
										md={4}
										sm={12}
										className='business-item'
										key={business.id}
									>
										<Paper className={classes.paper}>
											<img
												className='business-image'
												src='https://cdn.pixabay.com/photo/2016/02/19/11/19/office-1209640_960_720.jpg'
												alt='office'
											/>
											<h2>{business.brandName}</h2>
											<p className='text'>Location: {business.location}</p>
											<Button
												variant='contained'
												color='secondary'
												onClick={() => moreDetailsHandlers(business.id)}
											>
												More details
											</Button>
										</Paper>
									</Grid>
							  ))}
					</Grid>
				</Grid>
				{isOpenDialog && (
					<AlertDialog
						open={isOpenDialog}
						businessInfo={businessInfo}
						handleClose={handleClose}
					/>
				)}
			</div>
		</div>
	)
}

export default Business
