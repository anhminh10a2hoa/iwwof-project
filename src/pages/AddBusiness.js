import React, { useState } from 'react'
import MenuAppBar from '../components/UI/MenuAppBar'
import { useSelector } from 'react-redux'
import axios from 'axios'
import { BASE_URL } from '../constance/url'
import { useHistory } from 'react-router-dom'

function AddBusiness() {
	const userInfo = useSelector((state) => state.users.userInfo)
	let history = useHistory()
	const [business, setBusiness] = useState({
		additionalOffer: '',
		brandName: '',
		category: '',
		description: '',
		location: '',
		published: true,
		websites: '',
		user: { id: userInfo.id },
	})
	const handleChange = (event) => {
		event.preventDefault()
		const { name, value } = event.target
		setBusiness((prevState) => ({ ...prevState, [name]: value }))
	}

	const onChangeCheckBox = () => {
		console.log(business)
		setBusiness((prevState) => ({
			...prevState,
			published: !business.published,
		}))
	}

	const handleSubmit = async (event) => {
		console.log(business)
		event.preventDefault()
		await axios({
			method: 'POST',
			url: BASE_URL + 'business/add',
			data: JSON.stringify(business),
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
				Authorization: 'Bearer ' + userInfo.accessToken,
			},
		})
			.then((res) => {
				if (res.status === 200) {
					new Promise(() =>
						setTimeout(alert('Business Addedd Successfully!'), 2000)
					)
					history.push('/business')
				} else {
					alert('Add failed, try again!')
				}
			})
			.catch((err) => {
				console.log(err)
				alert('Add failed, try again!')
			})
	}
	return (
		<div className='add-business'>
			<MenuAppBar title='Add Business' />
			<div className='container-add-business'>
				<form id='contact' onSubmit={handleSubmit}>
					<h3>Add your own business</h3>
					<br />
					<fieldset>
						<input
							placeholder='Business name'
							type='text'
							tabIndex='1'
							required
							autoFocus
							name='brandName'
							value={business.brandName}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							placeholder='Category'
							type='text'
							tabIndex='2'
							required
							name='category'
							value={business.category}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							placeholder='Websites'
							type='url'
							tabIndex='3'
							required
							name='websites'
							value={business.websites}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<textarea
							placeholder='Additional offer'
							type='text'
							tabIndex='4'
							required
							name='additionalOffer'
							value={business.additionalOffer}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<textarea
							placeholder='Description'
							tabIndex='5'
							required
							name='description'
							value={business.description}
							onChange={handleChange}
						></textarea>
					</fieldset>
					<fieldset>
						<input
							placeholder='Location'
							type='text'
							tabIndex='6'
							required
							name='location'
							value={business.location}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							type='checkbox'
							name='published'
							value={business.published}
							onChange={onChangeCheckBox}
						/>
						<label htmlFor='published' className='label-publish'>
							{' '}
							Publish it now
						</label>
					</fieldset>
					<fieldset>
						<button
							name='submit'
							type='submit'
							id='contact-submit'
							data-submit='...Sending'
						>
							Submit
						</button>
					</fieldset>
				</form>
			</div>
		</div>
	)
}

export default AddBusiness
