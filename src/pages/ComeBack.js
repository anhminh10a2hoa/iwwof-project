import React from 'react'

function ComeBack() {
	return (
		<div className='container'>
			<div className='mainbox'>
				<div className='err'>4</div>
				<i className='far fa-question-circle fa-spin'></i>
				<div className='err2'>1</div>
				<div className='msg'>
					Maybe your account still in the waiting list? Please wait for us and
					come back later
					<p>
						If you have any question, let's{' '}
						<a href='https://iwwof.com/contact-us/'>contact</a> or go{' '}
						<a href='https://iwwof.com/'>home</a>.
					</p>
				</div>
			</div>
		</div>
	)
}

export default ComeBack
