import React, { useState } from 'react'
import MenuAppBar from '../components/UI/MenuAppBar'
import axios from 'axios'
import { BASE_URL } from '../constance/url'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { setRoles, setUser } from '../actions/userAction'

function Me() {
	const userInfo = useSelector((state) => state.users.userInfo)
	let history = useHistory()
	const dispatch = useDispatch()
	const [sure, setSure] = useState(false)
	const [userUpdated, setUserUpdate] = useState({
		allowByAdmin: 1,
		id: userInfo.id,
		email: userInfo.email ? userInfo.email : '',
		location: userInfo.location ? userInfo.location : '',
		name: userInfo.name ? userInfo.name : '',
		otherContactInfo: userInfo.otherContactInfo
			? userInfo.otherContactInfo
			: '',
		phoneNumber: userInfo.phoneNumber ? userInfo.phoneNumber : '',
		website: userInfo.website ? userInfo.website : '',
		roles: [
			{
				id: 0,
				name: userInfo.roles[0],
			},
		],
		username: userInfo.username,
		password: '',
	})
	const handleChange = (event) => {
		event.preventDefault()
		const { name, value } = event.target
		setUserUpdate((prevState) => ({ ...prevState, [name]: value }))
	}

	const onChangeCheckBox = () => {
		setSure(!sure)
		console.log(sure)
	}

	const handleSubmit = async (event) => {
		console.log(userUpdated)
		event.preventDefault()
		if (sure == false) {
			alert('Your need to make sure your action')
		} else {
			await axios
				.put(BASE_URL + 'admin/update/user', JSON.stringify(userUpdated), {
					headers: {
						Authorization: 'Bearer ' + userInfo.accessToken,
						'Content-Type': 'application/json;charset=UTF-8',
					},
				})
				.then((res) => {
					if (res.status === 200) {
						new Promise(() => setTimeout(alert('Add success!'), 2000))
						history.push('/me')
						localStorage.removeItem('user')
						dispatch(setRoles([]))
						dispatch(setUser({}))
						history.push('/login')
					} else {
						alert('Add failed, try again!')
					}
				})
				.catch((err) => {
					console.log(err)
					alert('Add failed, try again!')
				})
		}
	}
	return (
		<div>
			<MenuAppBar title={userInfo.username} />
			<div className='container-add-business'>
				<form id='contact' onSubmit={handleSubmit}>
					<h3>Update your information</h3>
					<br />
					<fieldset>
						<input
							placeholder='Your name'
							type='text'
							tabIndex='1'
							required
							autoFocus
							name='name'
							value={userUpdated.name}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							placeholder='Email'
							type='text'
							tabIndex='2'
							required
							name='email'
							value={userUpdated.email}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							placeholder='Phone number'
							type='text'
							tabIndex='3'
							name='phoneNumber'
							value={userUpdated.phoneNumber}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<textarea
							placeholder='Other contact info'
							tabIndex='4'
							name='otherContactInfo'
							value={userUpdated.otherContactInfo}
							onChange={handleChange}
						></textarea>
					</fieldset>
					<fieldset>
						<input
							placeholder='Website'
							type='url'
							tabIndex='5'
							name='website'
							value={userUpdated.website}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							placeholder='Location'
							type='text'
							tabIndex='6'
							name='location'
							value={userUpdated.location}
							onChange={handleChange}
						/>
					</fieldset>
					<fieldset>
						<input
							type='checkbox'
							name='sure'
							value={sure}
							onChange={onChangeCheckBox}
						/>
						<label htmlFor='sure' className='label-publish'>
							{' '}
							Are you sure to update your information?
						</label>
					</fieldset>
					<fieldset>
						<button
							name='submit'
							type='submit'
							id='contact-submit'
							data-submit='...Sending'
						>
							Submit
						</button>
					</fieldset>
					<a href='/update-password'>Change your password</a>
				</form>
			</div>
		</div>
	)
}

export default Me
