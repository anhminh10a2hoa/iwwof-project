import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import axios from 'axios'
import { checkSignUp } from '../utils/checkValidator'
import { useHistory } from 'react-router-dom'
import { BASE_URL } from '../constance/url'
import Logo from '../assets/logo.png'

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	logo: {
		height: '60px',
		position: 'absolute',
		width: '60px',
	},
}))

export default function SignUp() {
	const classes = useStyles()
	let history = useHistory()
	const [userSignUp, setUserSignUp] = useState({
		email: '',
		username: '',
		password: '',
		confirmPassword: '',
	})

	const handleChange = (event) => {
		const { name, value } = event.target
		setUserSignUp((prevState) => ({ ...prevState, [name]: value }))
	}

	const handleSubmit = async (event) => {
		event.preventDefault()
		checkSignUp(userSignUp)
		await axios({
			method: 'POST',
			url: BASE_URL + 'auth/signup',
			data: JSON.stringify({
				username: userSignUp.username,
				email: userSignUp.email,
				password: userSignUp.password,
			}),
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
		})
			.then((res) => {
				console.log(res)
				if (res.status === 200) {
					new Promise(() => setTimeout(alert('Register success!'), 2000))
					history.push('/login')
				} else {
					alert('Register failed, try again!')
					setUserSignUp({
						email: '',
						username: '',
						password: '',
						confirmPassword: '',
					})
				}
			})
			.catch((err) => {
				console.log(err)
				alert('Register failed, try again!')
				setUserSignUp({
					email: '',
					username: '',
					password: '',
					confirmPassword: '',
				})
			})
	}

	return (
		<Container component='main' maxWidth='xs'>
			<CssBaseline />
			<div className={classes.paper}>
				<img src={Logo} alt='' className={classes.logo} />
				<br />
				<br />
				<br />
				<Typography component='h1' variant='h5'>
					Sign up
				</Typography>
				<form onSubmit={handleSubmit} className={classes.form} noValidate>
					<TextField
						variant='outlined'
						margin='normal'
						required
						fullWidth
						id='email'
						label='Email Address'
						name='email'
						value={userSignUp.email}
						autoComplete='email'
						onChange={handleChange}
						autoFocus
						type='email'
					/>
					<TextField
						variant='outlined'
						margin='normal'
						required
						fullWidth
						id='username'
						value={userSignUp.username}
						label='Display name'
						name='username'
						onChange={handleChange}
					/>
					<TextField
						variant='outlined'
						margin='normal'
						required
						fullWidth
						name='password'
						label='Password'
						type='password'
						id='password'
						value={userSignUp.password}
						autoComplete='current-password'
						onChange={handleChange}
					/>
					<TextField
						variant='outlined'
						margin='normal'
						required
						fullWidth
						name='confirmPassword'
						label='Confirm password'
						type='password'
						id='password'
						value={userSignUp.confirmPassword}
						autoComplete='current-password'
						onChange={handleChange}
					/>
					<FormControlLabel
						control={<Checkbox value='remember' color='primary' />}
						label='Remember me'
					/>
					<Button
						type='submit'
						fullWidth
						variant='contained'
						color='primary'
						className={classes.submit}
					>
						Sign Up
					</Button>
					<Grid container>
						<Grid item>
							<Link href='/login' variant='body2'>
								{'Already have an account? Sign In'}
							</Link>
						</Grid>
					</Grid>
				</form>
			</div>
		</Container>
	)
}
