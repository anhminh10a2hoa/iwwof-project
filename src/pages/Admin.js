import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import UserPending from '../components/admin/UserPending'

function Admin() {
	const userRole = useSelector((state) => state.users.roles)
	return userRole.includes('ROLE_ADMIN') ? (
		<UserPending />
	) : (
		<Redirect to='/login' />
	)
}

export default Admin
