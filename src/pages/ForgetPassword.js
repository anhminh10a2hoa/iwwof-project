import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { BASE_URL } from '../constance/url'
import Logo from '../assets/logo.png'

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	logo: {
		height: '60px',
		position: 'absolute',
		width: '60px',
	},
}))

function ForgetPassword() {
	const classes = useStyles()
	let history = useHistory()
	const [forgetPassword, setForgetPassword] = useState({
		email: '',
		username: '',
		password: '',
		confirmPassword: '',
	})

	const handleChange = (event) => {
		const { name, value } = event.target
		setForgetPassword((prevState) => ({ ...prevState, [name]: value }))
	}

	const handleSubmit = async (event) => {
		event.preventDefault()
		await axios({
			method: 'POST',
			url:
				BASE_URL +
				'auth/forgotpassword?email=' +
				forgetPassword.email +
				'&username=' +
				forgetPassword.username,
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
		})
			.then((res) => {
				if (res.status === 200) {
					new Promise(() => setTimeout(alert(res.data), 2000))
					history.push('/login')
				} else {
					alert('Something went wrong!')
					setForgetPassword({ email: '', username: '' })
				}
			})
			.catch((err) => {
				console.log(err)
				alert('Something went wrong!')
				setForgetPassword({ email: '', username: '' })
			})
	}

	return (
		<Container component='main' maxWidth='xs'>
			<CssBaseline />
			<div className={classes.paper}>
				<img src={Logo} alt='' className={classes.logo} />
				<br />
				<br />
				<br />
				<Typography component='h1' variant='h5'>
					Forget password
				</Typography>
				<form onSubmit={handleSubmit} className={classes.form} noValidate>
					<TextField
						variant='outlined'
						margin='normal'
						required
						fullWidth
						id='email'
						label='Email Address'
						name='email'
						value={forgetPassword.email}
						autoComplete='email'
						onChange={handleChange}
						autoFocus
						type='email'
					/>
					<TextField
						variant='outlined'
						margin='normal'
						required
						fullWidth
						id='username'
						value={forgetPassword.username}
						label='Display name'
						name='username'
						onChange={handleChange}
					/>
					<Button
						type='submit'
						fullWidth
						variant='contained'
						color='primary'
						className={classes.submit}
					>
						Send
					</Button>
					<Grid container>
						<Grid item xs>
							<Link href='/login' variant='body2'>
								Back to login
							</Link>
						</Grid>
						<Grid item>
							<Link href='/signup' variant='body2'>
								{"Don't have an account? Sign Up"}
							</Link>
						</Grid>
					</Grid>
				</form>
			</div>
		</Container>
	)
}

export default ForgetPassword
