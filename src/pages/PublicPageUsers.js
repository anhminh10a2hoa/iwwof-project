import React, { useEffect, useState } from 'react'
import MenuAppBarPublic from '../components/UI/MenuAppBarPublic'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import UserDialog from '../components/UI/UserDialog'
import { BASE_URL } from '../constance/url'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: '#6886c5',
		'&:hover': {
			backgroundColor: '#f9f9f9',
		},
	},
}))

function PublicPage() {
	const classes = useStyles()
	const [listUser, setListUser] = useState([])
	const [loading, setLoading] = useState(true)
	const [isOpenDialog, setIsOpenDialog] = useState(false)
	const [userDialog, setUserInfo] = useState(1)

	useEffect(() => {
		axios.get(BASE_URL + 'public/user/all').then((res) => {
			let listUserApproved = res.data.filter((user) => user.allowByAdmin)
			setListUser(listUserApproved)
			setLoading(false)
		})
	}, [])

	const handleClose = () => {
		setIsOpenDialog(false)
	}

	const moreDetailsHandlers = (id) => {
		const user = listUser.find((b) => b.id == id)
		setUserInfo(user)
		setIsOpenDialog(true)
	}

	return (
		<div>
			<MenuAppBarPublic />
			<h1 className='title'>
				{listUser.length + ' '} users are joining our community now
			</h1>
			<div className='business-container'>
				<Grid container spacing={1}>
					<Grid container item lg={12} md={12} sm={12} spacing={6}>
						{listUser.map((user) => (
							<Grid
								item
								lg={3}
								md={4}
								sm={12}
								className='business-item'
								key={user.id}
							>
								<Paper className={classes.paper}>
									<img
										className='business-image'
										src='https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg'
										alt='office'
									/>
									<h2>{user.username}</h2>
									<p className='text'>Email: {user.email}</p>
									<Button
										variant='contained'
										color='secondary'
										onClick={() => moreDetailsHandlers(user.id)}
									>
										More details
									</Button>
								</Paper>
							</Grid>
						))}
					</Grid>
				</Grid>
				{isOpenDialog && (
					<UserDialog
						open={isOpenDialog}
						userInfo={userDialog}
						handleClose={handleClose}
					/>
				)}
			</div>
		</div>
	)
}

export default PublicPage
