import React from 'react'
import MenuAppBar from '../components/UI/MenuAppBar'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import FacebookIcon from '@material-ui/icons/Facebook'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import InstagramIcon from '@material-ui/icons/Instagram'
import LanguageIcon from '@material-ui/icons/Language'
import Filter1Icon from '@material-ui/icons/Filter1'
import Filter2Icon from '@material-ui/icons/Filter2'
import Filter3Icon from '@material-ui/icons/Filter3'
import Filter4Icon from '@material-ui/icons/Filter4'
import Filter5Icon from '@material-ui/icons/Filter5'
import Filter6Icon from '@material-ui/icons/Filter6'
import ContactMailIcon from '@material-ui/icons/ContactMail'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import { useSelector } from 'react-redux'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		height: '100vh',
		overflowX: 'hidden',
	},
	paper: {
		padding: theme.spacing(1),
		textAlign: 'center',
		color: theme.palette.text.secondary,
	},
	paper2: {
		padding: theme.spacing(1),
		textAlign: 'left',
		color: theme.palette.text.secondary,
		paddingLeft: '20px',
		paddingRight: '20px',
	},
	titleSocial: {
		marginBottom: '10px',
	},
	facebookIcon: {
		color: '#3b5998',
		cursor: 'pointer',
	},
	instagramIcon: {
		color: '#E1306C',
		cursor: 'pointer',
	},
	linkedInIcon: {
		color: '#0e76a8',
		cursor: 'pointer',
	},
	link: {
		color: 'black',
		'&:hover': {
			color: 'blue',
			textDecoration: 'none',
		},
	},
	websiteIcon: {
		color: '#3b5998',
		cursor: 'pointer',
		marginBottom: '-4.75px',
	},
	doneIcon: {
		color: 'green',
		cursor: 'pointer',
		marginBottom: '-4.75px',
	},
	titleEntrepreneur: {
		textAlign: 'center',
	},
}))

function MainPage() {
	const classes = useStyles()
	const userRole = useSelector((state) => state.users.roles)
	return (
		<div className={classes.root}>
			<MenuAppBar title='IWWOF' />
			<h1 className='title-main-page'>Welcome to the entrepreneur website</h1>
			<Grid container spacing={1}>
				<Grid container item xs={12} spacing={3}>
					<Grid item xs={6}>
						<Paper className={classes.paper}>
							<h2>About us</h2>
							<DialogContent>
								<DialogContentText>
									IWWOF was born as a Facebook digital networking group in
									August, 2019 to allow women from all walks of life to connect
									and support each other on their journey in Finland. We welcome
									women from all identities and backgrounds to join our vibrant
									community. Our Facebook group currently celebrates over 5000
									dedicated members.
								</DialogContentText>
							</DialogContent>
							<DialogContentText>
								<LanguageIcon className={classes.websiteIcon} />{' '}
								<a
									className={classes.link}
									href='https://iwwof.com'
									target='_blank'
									rel='noreferrer'
								>
									https://iwwof.com
								</a>
							</DialogContentText>
							<DialogContentText>
								<ContactMailIcon className={classes.websiteIcon} />{' '}
								contact@iwwof.com
							</DialogContentText>
							<DialogContentText>
								<LocationOnIcon className={classes.websiteIcon} /> Helsinki,
								Finland
							</DialogContentText>
							<DialogContent>
								<h4 className={classes.titleSocial}>Our social network</h4>
								<FacebookIcon
									className={classes.facebookIcon}
									onClick={() => {
										window.location.href =
											'https://www.facebook.com/groups/348015526109949/'
									}}
								/>
								<LinkedInIcon
									className={classes.linkedInIcon}
									onClick={() => {
										window.location.href =
											'https://www.linkedin.com/in/unavailable/'
									}}
								/>
								<InstagramIcon
									className={classes.instagramIcon}
									onClick={() => {
										window.location.href = 'https://www.instagram.com/iwwof_/'
									}}
								/>
							</DialogContent>
						</Paper>
					</Grid>
					<Grid item xs={6}>
						<Paper className={classes.paper2}>
							<h2 className={classes.titleEntrepreneur}>
								{userRole.includes('ROLE_ADMIN') ? 'Admin Guide' : 'User Guide'}
							</h2>
							<DialogContent>
								<Filter1Icon className={classes.doneIcon} />{' '}
								{userRole.includes('ROLE_ADMIN')
									? 'Update your profile'
									: 'Sign up to join the ‘IWWOF Entrepreneur Listing’'}
							</DialogContent>
							<DialogContent>
								<Filter2Icon className={classes.doneIcon} />{' '}
								{userRole.includes('ROLE_ADMIN')
									? 'Approve the new users from the User page'
									: 'Wait for approval from the admin'}
							</DialogContent>
							<DialogContent>
								<Filter3Icon className={classes.doneIcon} />{' '}
								{userRole.includes('ROLE_ADMIN')
									? 'Edit/Remove users from the User page'
									: 'Check your registered email for approval or rejection of request'}
							</DialogContent>
							<DialogContent>
								<Filter4Icon className={classes.doneIcon} />{' '}
								{userRole.includes('ROLE_ADMIN')
									? 'Reset User passwords from the User page'
									: 'Sign in and Update your profile'}
							</DialogContent>
							{!userRole.includes('ROLE_ADMIN') && (
								<DialogContent>
									<Filter5Icon className={classes.doneIcon} /> Add your business
									information
								</DialogContent>
							)}
							{!userRole.includes('ROLE_ADMIN') && (
								<DialogContent>
									<Filter6Icon className={classes.doneIcon} /> Keep your
									information updated
								</DialogContent>
							)}
						</Paper>
					</Grid>
				</Grid>
			</Grid>
		</div>
	)
}

export default MainPage
